module.exports = {
    context: __dirname + "/public/js",
    devtool  : 'eval-source-map',
    entry: "./client.js",

    output: {
        filename: "bundle.js",
        path: __dirname + "/public/js/dist"
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ["babel?presets[]=react,presets[]=es2015,presets[]=stage-0"]
            },
            {
                test: /\.scss$/,
                loaders: ["style", "css?sourceMap", "sass?sourceMap"]
            }
        ]
    }
};