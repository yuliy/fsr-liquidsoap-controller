const path = require('path');

const koa = require('koa');
const route = require('koa-route');
const Router = require('koa-router');
const serve = require('koa-static');
const handlebars = require("koa-handlebars");
const logger = require('koa-logger');

const config = require('./config');

const apiController = require('./controllers/api');
const authController = require('./controllers/auth');

var app = module.exports = koa();
app.use(logger());

app.use(serve(path.join(__dirname, 'public')));

app.use(handlebars({
    defaultLayout: "main",
    root: 'views',
    viewsDir: '.',
    layoutsDir: 'layouts',
    extension: 'handlebars'
}));

app.use(require('koa-body')({
    multipart: true
}));

const MemoryStore = require('koa-generic-session/lib/memory_store');
app.use(require('koa-generic-session')({
    store: new MemoryStore()
}));

// setup passport
app.keys = ['frkqwf09qwe_415adsfaer8*$*(Q)@)'];
require('./auth');
const passport = require('koa-passport');
app.use(passport.initialize());
app.use(passport.session());

var router = new Router();

/**
 * Internal API
 */
function* checkAuthorization(next) {
    if (this.isAuthenticated()) {
        yield next;
    }
    else {
        this.response.status = 403;
    }
}

router.post('/authenticate', authController.authenticate);
router.get('/logout', authController.logout);

router.get('/api/tracks', checkAuthorization, apiController.getTracksAction);
router.get('/api/track/:id', checkAuthorization, apiController.getTrackAction);
router.put('/api/track/:id', checkAuthorization, apiController.putTrackAction);
router.post('/api/track/head', checkAuthorization, apiController.headTrackAction);
router.get('/api/track/:id/upvote', checkAuthorization, apiController.upvoteTrackAction);
router.get('/api/track/:id/downvote', checkAuthorization, apiController.downvoteTrackAction);

router.get('/api/playlists', checkAuthorization, apiController.getPlaylistsAction);
router.get('/api/playlist/:id', checkAuthorization, apiController.getPlaylistAction);
router.post('/api/playlist', checkAuthorization, apiController.createPlaylistAction);
router.put('/api/playlist/:id', checkAuthorization, apiController.updatePlaylistAction);
router.put('/api/playlist/:id/tracks', checkAuthorization, apiController.updatePlaylistTracksAction);
router.put('/api/playlist/:id/track/:track_id', checkAuthorization, apiController.addTrackToPlaylistAction);
router.delete('/api/playlist/:id/track/:track_id', checkAuthorization, apiController.deleteTrackFromPlaylistAction);
router.get('/api/playlist/:id/export/:name', checkAuthorization, apiController.exportPlaylistAction);

/**
 * External API
 */
function* checkExternalAuthorization(next) {
    if (this.headers['api-key'] && this.headers['api-key'] === config.api_key) {
        yield next;
    } else {
        this.response.status = 403;
    }

}

router.get('/api/external/tracks', checkExternalAuthorization, apiController.getTracksAction);
router.get('/api/external/track/:id', checkExternalAuthorization, apiController.getTrackAction);
router.post('/api/external/track/head', checkExternalAuthorization, apiController.headTrackAction);
router.post('/api/external/track/sync', checkExternalAuthorization, apiController.upsertTrackFilesAction);

app.use(router.routes());

app.use(
    route.get(
        '/*',
        function *() {
            yield this.render("index", {});
        }
    )
);

if (!module.parent) {
    app.listen(config.port);
}