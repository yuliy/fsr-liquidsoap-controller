var bookshelf = require('./bookshelf');
var User = bookshelf.Model.extend({
    tableName: 'admin_user'
});
var Tag = bookshelf.Model.extend({
    tableName: 'tag'
});
var Track = bookshelf.Model.extend({
    tableName: 'track',
    tags: function () {
        return this.belongsToMany(Tag);
    },

    getRadioLink: function() {
        return "https://fsrdata.s3.amazonaws.com/radio/" + this.get('media_id') + "_radio.mp3";
    },

    getM3ULine: function () {
        return 'annotate:title="' + this.get('name') + '",artist="' + this.get('id') + '":' + this.getRadioLink();
    }
});
var Playlist = bookshelf.Model.extend({
    tableName: 'playlist',
    tracks: function () {
        return this.belongsToMany(Track);
    }
});

exports.User = User;
exports.Tag = Tag;
exports.Track = Track;
exports.Playlist = Playlist;