var plan = require('flightplan');

function buildDependencyExports() {
    var exports = [
        'export FREESOUNDS_DB_HOST=' + process.env.FREESOUNDS_DB_HOST,
        'export FREESOUNDS_DB_PORT=' + process.env.FREESOUNDS_DB_PORT,
        'export FREESOUNDS_DB_NAME=' + process.env.FREESOUNDS_DB_NAME,
        'export FREESOUNDS_DB_USER=' + process.env.FREESOUNDS_DB_USER,
        'export FREESOUNDS_DB_PASS=' + process.env.FREESOUNDS_DB_PASS,
        'export AWS_ACCESS_TOKEN=' + process.env.AWS_ACCESS_TOKEN,
        'export AWS_ACCESS_SECRET=' + process.env.AWS_ACCESS_SECRET
    ];
    return exports.join(";");
}

plan.target('production', [
    {
        host: '159.203.71.17',
        username: 'www',
        //agent: process.env.SSH_AUTH_SOCK
        privateKey: '/home/runner/.ssh/id_rsa'
    }
]);

var tmpDir = 'admin-' + new Date().getTime();

plan.local(function(local) {
    local.log('Run build');
    local.exec('node_modules/webpack/bin/webpack.js --progress --colors');

    var filesToCopy = local.exec('git ls-files', {silent: true}).stdout.split('\n');
    var result1 = local.find('public/js/dist -type f', {silent: true}).stdout.split('\n');
    var result2 = local.find('ecosystem.json -type f', {silent: true}).stdout.split('\n');
    local.transfer(filesToCopy.concat(result1).concat(result2), '/tmp/' + tmpDir);
});

plan.remote(function(remote) {
    remote.log('Move folder to web root');
    remote.sudo('cp -R /tmp/' + tmpDir + ' ~', {user: 'www'});
    remote.rm('-rf /tmp/' + tmpDir);
    remote.with('. /home/www/.nvm/nvm.sh', function() {
        remote.exec('npm set progress=false');
        remote.sudo('npm --production --prefix ~/' + tmpDir + ' install ~/' + tmpDir, {user: 'www'});

        remote.log('Reload application');
        remote.with('cd ~/' + tmpDir, function() {
            remote.with(buildDependencyExports(), function() {
                remote.exec('pm2 delete app');

                remote.exec('pm2 startOrRestart ~/' + tmpDir + '/ecosystem.json');

                remote.exec('pm2 list');
            });
        });
    });
});