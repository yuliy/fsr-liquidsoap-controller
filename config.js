var config = {};

config.port = 3131;

/** */
config.jwt_secret = "superSecret";

/** External API Configuration */
config.api_key = 'bad_keys';

/** Crypto Configuration */
config.crypto = {};
config.crypto.key = process.env.FSR_CONTROLLER_KEY;
config.crypto.hash_header = 'Chao-Hash';

/** Postgres Configuration */
config.db = {};
config.db.host = process.env.FREESOUNDS_DB_HOST;
config.db.port = process.env.FREESOUNDS_DB_PORT;
config.db.name = process.env.FREESOUNDS_DB_NAME;
config.db.user = process.env.FREESOUNDS_DB_USER;
config.db.pass = process.env.FREESOUNDS_DB_PASS;

/** S3 Configuration */
config.s3 = {};
config.s3.aws_token = process.env.AWS_ACCESS_TOKEN;
config.s3.aws_secret = process.env.AWS_ACCESS_SECRET;

/** Telnet Configuration */
config.liquidsoap_telnet = {};
config.liquidsoap_telnet.host = '127.0.0.1';
config.liquidsoap_telnet.port = '1234';

module.exports = config;