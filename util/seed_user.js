var minimist = require('minimist');

var orm = require('../orm');
var secure_service = require('../services/secure_service');

var argv = minimist(process.argv.slice(2));

if (argv.username && argv.password) {
    orm.User.where('username', '=', argv.username)
        .fetch()
        .then((user) => {
            if (!user) {
                secure_service.bcryptHash(argv.password, 10).then((hash) => {
                    orm.User.forge(
                        {
                            username: argv.username,
                            password: hash
                        }
                    ).save().then(() => {
                        console.log("user successfully seeded");
                    });
                });
            } else {
                console.log('user already exists...');
                secure_service.bcryptCompare(argv.password, user.get('password')).then(matches => {
                    if (matches) {
                        console.log('user already has that password...');
                    } else {
                        secure_service.bcryptHash(argv.password, 10).then(hash => {
                            user.set({
                                password: hash
                            }).save().then((user) => {
                                console.log('password reset');
                            });
                        })
                    }
                })
            }
        })
}