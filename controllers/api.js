var net = require('net');

var t = require('tcomb');
var validation = require('tcomb-validation');
var validate = validation.validate;

var track_service = require('../services/track_service');
var playlist_service = require('../services/playlist_service');

class ApiError extends Error {
    constructor(code, message, status) {
        code = code || 'ERR_SERVER';
        message = message || 'SERVER ERROR';

        super(message, code);

        Error['captureStackTrace'](this, ApiError);

        this.name = 'ApiError';
        this.code = code;
        this.message = message;
        this.expose = true;
        this.status = status || 500;
    }
}

function* getTrackAction(next) {
    try {
        this.body = yield track_service.getTrack(this.params.id);
    } catch (e) {
        throw new ApiError('Not Found', "Track Not Found", 404);
    }
}

function* headTrackAction() {
    const HeadTrackBody = t.struct({
        media_id: t.Str
    });

    var validation = validate(this.request.body, HeadTrackBody);
    if (!validation.isValid()) {
        throw new ApiError('ERR_ARGUMENTS', validation.firstError().message, 400);
    }

    this.body = yield track_service.headTrack(this.request.body.media_id);
}

function* upsertTrackFilesAction() {
    const HeadTrackBody = t.struct({
        media_id: t.Str
    });

    var validation = validate(this.request.body, HeadTrackBody);
    if (!validation.isValid()) {
        throw new ApiError('ERR_ARGUMENTS', validation.firstError().message, 400);
    }

    this.body = yield track_service.upsertTrackFiles(this.request.body.media_id);
}

function* putTrackAction(next) {
    const PutTrackBody = t.struct({
        name: t.Str,
        tags: t.list(t.Str)
    });

    var validation = validate(this.request.body, PutTrackBody);
    if (!validation.isValid()) {
        throw new ApiError('ERR_ARGUMENTS', validation.firstError().message, 400);
    }

    this.body = yield track_service.updateTrack(this.params.id, this.request.body);
}

function* upvoteTrackAction(next) {
    this.body = yield track_service.upvoteTrack(this.params.id);
}

function* downvoteTrackAction(next) {
    this.body = yield track_service.downvoteTrack(this.params.id);
}

function* getTracksAction() {
    this.body = yield track_service.getTracks();
    //this.type = 'application/json';
}

function* getPlaylistAction(next) {
    this.body = yield playlist_service.getPlaylist(this.params.id);
}

function* createPlaylistAction() {
    const CreatePlaylistBody = t.struct({
        name: t.Str
    });

    var validation = validate(this.request.body, CreatePlaylistBody);
    if (!validation.isValid()) {
        throw new ApiError('ERR_ARGUMENTS', validation.firstError().message, 400);
    }

    this.body = yield playlist_service.createPlaylist(this.request.body);
}

function* updatePlaylistAction(next) {
    const UpdatePlaylistBody = t.struct({
        name: t.Str
    });

    var validation = validate(this.request.body, UpdatePlaylistBody);
    if (!validation.isValid()) {
        throw new ApiError('ERR_ARGUMENTS', validation.firstError().message, 400);
    }

    this.body = yield playlist_service.updatePlaylist(this.params.id, this.request.body)
}

function* updatePlaylistTracksAction(next) {
    const Integer = t.refinement(t.Number, (n) => n % 1 === 0, 'Integer');
    const UpdatePlaylistTracksBody = t.struct({
        ids: t.list(Integer)
    });

    var validation = validate(this.request.body, UpdatePlaylistTracksBody);
    if (!validation.isValid()) {
        throw new ApiError('ERR_ARGUMENTS', validation.firstError().message, 400);
    }

    this.body = yield playlist_service.updatePlaylistTracks(this.params.id, this.request.body.ids);
}

function* addTrackToPlaylistAction(next) {
    this.body = yield playlist_service.addTrackToPlaylist(this.params.id, this.params.track_id);
}

function* deleteTrackFromPlaylistAction(next) {
    this.body = yield playlist_service.deleteTrackFromPlaylist(this.params.id, this.params.track_id);
}

function* exportPlaylistAction(next) {
    this.body = yield playlist_service.exportPlaylist(this.params.id, this.params.name);
}

function* getPlaylistsAction() {
    this.body = yield playlist_service.getPlaylists();
}

//var getLiquidsoapCommandList = function (request, response) {
//    var client = net.connect({port: 1234},
//        function () {
//            client.write('help\r\n');
//        }
//    );
//    var closed = false;
//    client.on('data', function (data) {
//        if (!closed) {
//            var toDump = data.toString();
//            toDump = _.split(toDump, "\r\n");
//            toDump = _.filter(toDump, function (item) {
//                return item[0] === "|";
//            });
//            toDump = _.map(toDump, function (item) {
//                return _.trimStart(item, '| ');
//            });
//            response.send(toDump);
//
//            client.write("quit\r\n");
//
//            closed = true;
//        }
//    });
//    client.on('end', function () {
//        console.log('disconnected from server');
//    });
//};

exports.getTrackAction = getTrackAction;
exports.headTrackAction = headTrackAction;
exports.upsertTrackFilesAction = upsertTrackFilesAction;
exports.putTrackAction = putTrackAction;
exports.upvoteTrackAction = upvoteTrackAction;
exports.downvoteTrackAction = downvoteTrackAction;
exports.getTracksAction = getTracksAction;

exports.getPlaylistAction = getPlaylistAction;
exports.createPlaylistAction = createPlaylistAction;
exports.updatePlaylistAction = updatePlaylistAction;
exports.updatePlaylistTracksAction = updatePlaylistTracksAction;
exports.addTrackToPlaylistAction = addTrackToPlaylistAction;
exports.deleteTrackFromPlaylistAction = deleteTrackFromPlaylistAction;
exports.exportPlaylistAction = exportPlaylistAction;
exports.getPlaylistsAction = getPlaylistsAction;

//exports.getLiquidsoapCommandList = getLiquidsoapCommandList;
