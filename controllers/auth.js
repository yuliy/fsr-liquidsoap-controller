var passport = require('koa-passport');

function* authenticate() {
    const ctx = this;
    yield passport.authenticate('local', function*(err, user, info) {
        if (err) throw err;

        if (user === false) {
            ctx.response.status = 403;
        } else {
            yield [ctx.login(user)];
            ctx.response.status = 200;
        }
    })
}

function* logout() {
    this.logout();
    this.response.status = 200;
}

exports.authenticate = authenticate;
exports.logout = logout;