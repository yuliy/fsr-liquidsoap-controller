var config = require('./config');

var knex = require('knex')({
    client: 'pg',
    connection: {
        host: config.db.host,
        port: config.db.port,
        database: config.db.name,
        user: config.db.user,
        password: config.db.pass
    }
});
module.exports = require('bookshelf')(knex);