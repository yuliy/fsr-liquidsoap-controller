require('../scss/main.scss');

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute} from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import App from './app/app.js';
import Home from './app/pages/Home';
import Playlist from './app/pages/Playlist';
import Liquidsoap from './app/pages/Liquidsoap';
import Login from './app/pages/Login';
import Logout from './app/pages/Logout';

import configureStore from './app/configureStore';
import history from './app/history';

const initialState = {
    playlists: {
        items: []
    },
    tracks: {
        tracks: []
    },
    playlist: {
        isFetching: false,
        playlist: {
            name: '',
            id: 0,
            tracks: []
        }
    }
};

const store = configureStore(initialState);

const syncHistory = syncHistoryWithStore(history, store);

ReactDOM.render((
    <Provider store={store}>
        <Router history={syncHistory}>
            <Route path="/" component={App}>
                <IndexRoute component={Home}/>
                <Route path="login" component={Login}/>
                <Route path="playlist/:playlistId" component={Playlist}/>
                <Route path="liquidsoap" component={Liquidsoap}/>
                <Route path="logout" component={Logout}/>
            </Route>
        </Router>
    </Provider>
), document.getElementById('app'));