import React from 'react';
import { connect } from 'react-redux';

import Header from './components/Header';
import Notification from './components/Notification';

import {fetchLogout} from './actions';

class App extends React.Component {
    render() {
        return (
            <div>
                <Header handleLogout={this.props.fetchLogout} />
                <Notification notifications={this.props.notifications} />
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        notifications: state.notifications.notifications
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchLogout: () => {
            dispatch(fetchLogout());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);