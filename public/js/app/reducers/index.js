import _ from 'lodash';

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import {
    SHOW_NOTIFICATION,

    REQUEST_TOKEN, RECEIVE_TOKEN,

    REQUEST_PLAYLISTS, RECEIVE_PLAYLISTS,
    REQUEST_PLAYLIST, RECEIVE_PLAYLIST,
    REQUEST_UPDATE_PLAYLIST, RECEIVE_UPDATE_PLAYLIST,
    REQUEST_ADD_TRACK_TO_PLAYLIST, RECEIVE_ADD_TRACK_TO_PLAYLIST,
    REQUEST_DEL_TRACK_FROM_PLAYLIST, RECEIVE_DEL_TRACK_FROM_PLAYLIST,
    REQUEST_BULK_ADD_TRACKS_TO_PLAYLIST, RECEIVE_BULK_ADD_TRACKS_TO_PLAYLIST,
    REQUEST_EXPORT_PLAYLIST, RECEIVE_EXPORT_PLAYLIST,

    REQUEST_TRACKS, RECEIVE_TRACKS,
    REQUEST_UPDATE_TRACK, RECEIVE_UPDATE_TRACK,
    REQUEST_UPVOTE_TRACK, RECEIVE_UPVOTE_TRACK,
    REQUEST_DOWNVOTE_TRACK, RECEIVE_DOWNVOTE_TRACK
} from "../actions";

const notifications = (state = {
    notifications: []
}, action) => {
    switch (action.type) {
        case SHOW_NOTIFICATION:
            return Object.assign({}, state, {
                isFetchingToken: true,
                notifications: _.concat(state.notifications, action.notification)
            });
        default:
            return state;
    }
};

const login = (state = {
    isFetchingToken: false,
    token: null
}, action) => {
    switch (action.type) {
        case REQUEST_TOKEN:
            return Object.assign({}, state, {
                isFetchingToken: true
            });
        case RECEIVE_TOKEN:
            return Object.assign({}, state, {
                isFetchingToken: false,
                token: action.token
            });
        default:
            return state
    }
};

const playlists = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
}, action) => {
    switch (action.type) {
        case REQUEST_PLAYLISTS:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_PLAYLISTS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.playlists
            });
        default:
            return state
    }
};

const playlist = (state = {
    isFetching: false,
    playlist: null
}, action) => {
    switch (action.type) {
        case REQUEST_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false,
                playlist: action.playlist
            });
        case REQUEST_UPDATE_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_UPDATE_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false,
                playlist: action.playlist
            });
        case REQUEST_ADD_TRACK_TO_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_ADD_TRACK_TO_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false,
                playlist: action.playlist
            });
        case REQUEST_DEL_TRACK_FROM_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_DEL_TRACK_FROM_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false,
                playlist: action.playlist
            });
        case REQUEST_BULK_ADD_TRACKS_TO_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_BULK_ADD_TRACKS_TO_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false,
                playlist: action.playlist
            });
        case REQUEST_EXPORT_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_EXPORT_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
};

const tracks = (state = {
    isFetching: false,
    tracks: []
}, action) => {
    const patchTracks = (track) => {
        const {id} = track;
        return _.map(state.tracks, (item) => {
            if (item.id === id) {
                return track
            }
            return item;
        });
    };

    switch (action.type) {
        case REQUEST_TRACKS:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_TRACKS:
            return Object.assign({}, state, {
                isFetching: false,
                tracks: action.tracks
            });
        case REQUEST_UPDATE_TRACK:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_UPDATE_TRACK:
            return Object.assign({}, state, {
                isFetching: false,
                tracks: patchTracks(action.track)
            });
        case REQUEST_UPVOTE_TRACK:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_UPVOTE_TRACK:
            return Object.assign({}, state, {
                isFetching: false,
                tracks: patchTracks(action.track)
            });
        case REQUEST_DOWNVOTE_TRACK:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_DOWNVOTE_TRACK:
            return Object.assign({}, state, {
                isFetching: false,
                tracks: patchTracks(action.track)
            });
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    login,
    playlists,
    tracks,
    playlist,
    notifications,
    routing: routerReducer
});

export default rootReducer;