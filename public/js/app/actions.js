import _ from 'lodash';
import axios from 'axios';

import history from './history';

export const SHOW_NOTIFICATION = "SHOW_NOTIFICATION";

export const REQUEST_TOKEN = "REQUEST_TOKEN";
export const RECEIVE_TOKEN = "RECEIVE_TOKEN";

export const REQUEST_LOGOUT = "REQUEST_LOGOUT";
export const RECEIVE_LOGOUT = "RECEIVE_LOGOUT";

export const REQUEST_PLAYLISTS = "REQUEST_PLAYLISTS";
export const RECEIVE_PLAYLISTS = "RECEIVE_PLAYLISTS";

export const REQUEST_PLAYLIST = "REQUEST_PLAYLIST";
export const RECEIVE_PLAYLIST = "RECEIVE_PLAYLIST";

export const REQUEST_UPDATE_PLAYLIST = "REQUEST_UPDATE_PLAYLIST";
export const RECEIVE_UPDATE_PLAYLIST = "RECEIVE_UPDATE_PLAYLIST";

export const REQUEST_ADD_TRACK_TO_PLAYLIST = "REQUEST_ADD_TRACK_TO_PLAYLIST";
export const RECEIVE_ADD_TRACK_TO_PLAYLIST = "RECEIVE_ADD_TRACK_TO_PLAYLIST";

export const REQUEST_DEL_TRACK_FROM_PLAYLIST = "REQUEST_DEL_TRACK_FROM_PLAYLIST";
export const RECEIVE_DEL_TRACK_FROM_PLAYLIST = "RECEIVE_DEL_TRACK_FROM_PLAYLIST";

export const REQUEST_BULK_ADD_TRACKS_TO_PLAYLIST = "REQUEST_BULK_ADD_TRACKS_TO_PLAYLIST";
export const RECEIVE_BULK_ADD_TRACKS_TO_PLAYLIST = "RECEIVE_BULK_ADD_TRACKS_TO_PLAYLIST";

export const REQUEST_EXPORT_PLAYLIST = "REQUEST_EXPORT_PLAYLIST";
export const RECEIVE_EXPORT_PLAYLIST = "RECEIVE_EXPORT_PLAYLIST";

export const REQUEST_TRACKS = "REQUEST_TRACKS";
export const RECEIVE_TRACKS = "RECEIVE_TRACKS";

export const REQUEST_UPDATE_TRACK = "REQUEST_UPDATE_TRACK";
export const RECEIVE_UPDATE_TRACK = "RECEIVE_UPDATE_TRACK";

export const REQUEST_UPVOTE_TRACK = "REQUEST_UPVOTE_TRACK";
export const RECEIVE_UPVOTE_TRACK = "RECEIVE_UPVOTE_TRACK";

export const REQUEST_DOWNVOTE_TRACK = "REQUEST_DOWNVOTE_TRACK";
export const RECEIVE_DOWNVOTE_TRACK = "RECEIVE_DOWNVOTE_TRACK";

const unauthorizedResponse = (dispatch) => {
    const notification = {
        id: new Date().getTime(),
        message: `Please Login to View`,
        type: 'error'
    };

    dispatch(addNotification(notification));

    history.pushState(null, '/login');
};

const showNotification = (notification) => {
    return {
        type: SHOW_NOTIFICATION,
        notification: notification
    };
};

export const addNotification = (notification) => {
    return (dispatch, getState) => {
        dispatch(showNotification(notification));
    };
};

const requestToken = () => {
    return {
        type: REQUEST_TOKEN
    };
};

const receiveToken = (data) => {
    return {
        type: RECEIVE_TOKEN,
        token: data.token
    };
};

export const fetchToken = (username, password) => {
    return (dispatch, getState) => {
        dispatch(requestToken());
        return axios.post('/authenticate', {username: username, password: password})
            .then((response) => {
                dispatch(receiveToken(response.data));
                history.pushState(null, `/`)
            })
            .catch(() => {
                console.log("ERROR");
                console.log(arguments);
            });
    };
};

const requestLogout = () => {
    return {
        type: REQUEST_LOGOUT
    };
};

const receiveLogout = () => {
    return {
        type: RECEIVE_LOGOUT
    };
};

export const fetchLogout = () => {
    return (dispatch, getState) => {
        dispatch(requestLogout());
        return axios.get('/logout')
            .then((response) => {
                dispatch(receiveLogout());
                history.pushState(null, '/login');
            })
            .catch(() => {
                console.log("ERROR");
                console.log(arguments);
            });
    };
};

function requestPlaylists() {
    return {
        type: REQUEST_PLAYLISTS
    }
}

function receivePlaylists(response) {
    return {
        type: RECEIVE_PLAYLISTS,
        playlists: response
    }
}

export function fetchPlaylists() {
    return (dispatch, getState) => {
        dispatch(requestPlaylists());
        try {
            return axios.get(`/api/playlists`)
                .then((response) => {
                    return response.data
                })
                .then((data) => dispatch(receivePlaylists(data)))
                .catch((reason) => {
                    unauthorizedResponse(dispatch);
                });
        } catch (e) {
            history.pushState(null, '/login');
        }
    };
}

function requestPlaylist() {
    return {
        type: REQUEST_PLAYLIST
    }
}

function receivePlaylist(response) {
    return {
        type: RECEIVE_PLAYLIST,
        playlist: response
    }
}

export function fetchPlaylist(id) {
    return (dispatch, getState) => {
        dispatch(requestPlaylist());
        return axios.get(`/api/playlist/${id}`)
            .then((response) => {
                dispatch(receivePlaylist(response.data));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
}

const requestUpdatePlaylist = () => {
    return {
        type: REQUEST_UPDATE_PLAYLIST
    };
};

const receiveUpdatePlaylist = (response) => {
    return {
        type: RECEIVE_UPDATE_PLAYLIST,
        playlist: response
    };
};

export const updatePlaylist = (playlist_id, update) => {
    return (dispatch, getState) => {
        dispatch(requestUpdatePlaylist());
        return axios.put(`/api/playlist/${playlist_id}`, update)
            .then((response) => {
                dispatch(receiveUpdatePlaylist(response.data));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestAddTrackToPlaylist = () => {
    return {
        type: REQUEST_ADD_TRACK_TO_PLAYLIST
    };
};

const receiveAddTrackToPlaylist = (response) => {
    return {
        type: RECEIVE_ADD_TRACK_TO_PLAYLIST,
        playlist: response
    };
};

export const addTrackToPlaylist = (playlist_id, track_id) => {
    return (dispatch, getState) => {
        dispatch(requestAddTrackToPlaylist());
        return axios.put(`/api/playlist/${playlist_id}/track/${track_id}`)
            .then((response) => {
                dispatch(receiveAddTrackToPlaylist(response.data));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestDelTrackFromPlaylist = () => {
    return {
        type: REQUEST_DEL_TRACK_FROM_PLAYLIST
    };
};

const receiveDelTrackFromPlaylist = (response) => {
    return {
        type: RECEIVE_DEL_TRACK_FROM_PLAYLIST,
        playlist: response
    };
};

export const delTrackFromPlaylist = (playlist_id, track_id) => {
    return (dispatch, getState) => {
        dispatch(requestDelTrackFromPlaylist());
        return axios.delete(`/api/playlist/${playlist_id}/track/${track_id}`)
            .then((response) => {
                dispatch(receiveDelTrackFromPlaylist(response.data));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestBulkAddTracksToPlaylist = () => {
    return {
        type: REQUEST_BULK_ADD_TRACKS_TO_PLAYLIST
    };
};

const receiveBulkAddTracksToPlaylist = (response) => {
    return {
        type: RECEIVE_DEL_TRACK_FROM_PLAYLIST,
        playlist: response
    };
};

export const bulkAddTracksToPlaylist = (playlist_id, ids) => {
    return (dispatch, getState) => {
        dispatch(requestBulkAddTracksToPlaylist());

        return axios.put(`/api/playlist/${playlist_id}/tracks`, {ids: ids})
            .then((response) => {
                dispatch(receiveBulkAddTracksToPlaylist(response.data));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestExportPlaylist = () => {
    return {
        type: REQUEST_EXPORT_PLAYLIST
    };
};

const receiveExportPlaylist = () => {
    return {
        type: RECEIVE_EXPORT_PLAYLIST
    }
};

export const exportPlaylist = (playlist_id, exportName) => {
    return (dispatch, getState) => {
        dispatch(requestExportPlaylist());
        return axios.get(`/api/playlist/${playlist_id}/export/${exportName}`)
            .then((response) => {
                dispatch(receiveExportPlaylist());
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestTracks = () => {
    return {
        type: REQUEST_TRACKS
    };
};

const receiveTracks = (response) => {
    return {
        type: RECEIVE_TRACKS,
        tracks: response
    };
};

export const fetchTracks = () => {
    return (dispatch, getState) => {
        dispatch(requestTracks());
        return axios.get(`/api/tracks`)
            .then((response) => {
                dispatch(receiveTracks(response.data));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestUpdateTrack = () => {
    return {
        type: REQUEST_UPDATE_TRACK
    };
};

const receiveUpdateTrack = (response) => {
    return {
        type: RECEIVE_UPDATE_TRACK,
        track: response
    }
};

export const updateTrack = (track_id, update) => {
    return (dispatch, getState) => {
        dispatch(requestUpdateTrack());
        return axios.put(`/api/track/${track_id}`, update)
            .then((response) => {
                const updatedTrack = response.data;
                dispatch(receiveUpdateTrack(updatedTrack));

                const notification = {
                    id: new Date().getTime(),
                    message: `Updated Track ${updatedTrack.name} (${updatedTrack.id})`,
                    type: 'success'
                };

                dispatch(addNotification(notification));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestUpvoteTrack = () => {
    return {
        type: REQUEST_UPVOTE_TRACK
    };
};

const receiveUpvoteTrack = (response) => {
    return {
        type: RECEIVE_UPVOTE_TRACK,
        track: response
    }
};

export const upvoteTrack = (track_id) => {
    return (dispatch, getState) => {
        dispatch(requestUpvoteTrack());
        return axios.get(`/api/track/${track_id}/upvote`)
            .then((response) => {
                const updatedTrack = response.data;
                dispatch(receiveUpvoteTrack(updatedTrack));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};

const requestDownvoteTrack = () => {
    return {
        type: REQUEST_DOWNVOTE_TRACK
    };
};

const receiveDownvoteTrack = (response) => {
    return {
        type: RECEIVE_DOWNVOTE_TRACK,
        track: response
    }
};

export const downvoteTrack = (track_id) => {
    return (dispatch, getState) => {
        dispatch(requestDownvoteTrack());
        return axios.get(`/api/track/${track_id}/downvote`)
            .then((response) => {
                const updatedTrack = response.data;
                dispatch(receiveDownvoteTrack(updatedTrack));
            })
            .catch((reason) => {
                unauthorizedResponse(dispatch);
            });
    };
};