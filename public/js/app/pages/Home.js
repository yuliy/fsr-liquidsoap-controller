import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router';

import axios from 'axios';
import _ from 'lodash';

import {fetchPlaylists} from '../actions';

import Header from '../components/Header';

class PlaylistsContainer extends React.Component {
    static propTypes = {
        playlists: React.PropTypes.arrayOf(React.PropTypes.shape({
            id: React.PropTypes.number.isRequired,
            name: React.PropTypes.string.isRequired,
            tracks: React.PropTypes.array.isRequired
        })).isRequired
    };

    static defaultProps = {
        playlists: []
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {playlists} = this.props;

        const PlaylistItem = (item) => {
            return (
                <li className="list-group-item">
                    <Link to={`/playlist/${item.id}`}>{item.name} Details</Link> <span
                    className="badge">{item.tracks.length} Tracks</span>
                </li>
            );
        };
        const playlistLister = _.map(playlists, (item) => {
            return <PlaylistItem key={item.id} {...item} />;
        });

        return (
            <div className="col-md-6">
                <div className="page-header">
                    <h2>All Playlists
                        <small>{playlists.length} Playlists</small>
                    </h2>
                </div>
                <div className="input-group columnControls">
                    <button type="button" className="btn btn-default" onClick={(e) => console.log("lol")}>
                        <span className="glyphicon glyphicon-plus" aria-hidden="true"/> Add New
                    </button>
                </div>
                <ul className="list-group">
                    {playlistLister}
                </ul>
            </div>
        )
    }
}

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {fetchPlaylists} = this.props;
        fetchPlaylists();
    }

    render() {
        const {playlists} = this.props;
        return (
            <div>
                <div className="container-fluid">
                    <div className="panel panel-default">
                        <div className="panel-body row">
                            <PlaylistsContainer playlists={playlists}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        playlists: state.playlists.items
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPlaylists: () => {
            dispatch(fetchPlaylists());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);