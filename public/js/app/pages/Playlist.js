import React from 'react';
import Loader from 'react-loader-advanced';
import _ from 'lodash';
import { connect } from 'react-redux';

import PlaylistContainer from '../components/PlaylistContainer';
import TracklistContainer from '../components/TracklistContainer';

import {fetchPlaylist, updatePlaylist, fetchTracks, addTrackToPlaylist, delTrackFromPlaylist, bulkAddTracksToPlaylist, exportPlaylist, updateTrack, upvoteTrack, downvoteTrack} from '../actions';

var t = require('tcomb-form');
var Form = t.form.Form;

const PlaylistModel = t.struct({
    name: t.maybe(t.String)
});

class UpdatePlaylist extends React.Component {
    constructor(props) {
        super(props);
    }

    exportPlaylist(e) {
        e.preventDefault();
        this.props.exportPlaylist(this.refs.playlistNameInput.value);
        this.refs.playlistNameInput.value = "";
    }

    updatePlaylist(e) {
        e.preventDefault();
        const {updatePlaylist} = this.props;
        updatePlaylist(this.refs.form.getValue());
    }

    render() {
        const {playlist, loading} = this.props;

        if (!playlist) return <div></div>;

        const value = {
            name: playlist.name
        };

        return (
            <div className="col-md-6">
                <Loader show={loading} message={'loading'}>
                    <div className="page-header">
                        <h4>{playlist.name}
                            <small>{playlist.tracks.length} Tracks</small>
                        </h4>
                    </div>

                    <div className="playlist-form-container">
                        <Form
                            ref="form"
                            value={value}
                            type={PlaylistModel}
                            options={{}}
                        />
                    </div>

                    <div className="btn-group" style={{paddingTop: '10px', width: '100%', paddingBottom: '10px'}}>
                        <button className="btn btn-secondary" type="button" style={{width: '100%'}}
                                onClick={(e) => this.updatePlaylist(e)}>Update
                        </button>
                    </div>

                    <div className="input-group columnControls">
                        <span className="input-group-btn">
                            <button className="btn btn-default" type="button" onClick={(e) => this.exportPlaylist(e)}>
                                Export As
                            </button>
                        </span>
                        <input ref="playlistNameInput" type="text" className="form-control"
                               placeholder="Playlist Name"/>
                    </div>
                </Loader>
            </div>
        );
    }
}

class CurrentTrack extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.refs.audioContainer) {
            this.refs.audioContainer.play();
        }
    }

    render() {
        const {currentTrack} = this.props;

        const audioContainer = currentTrack ? (
            <audio ref="audioContainer" controls
                   src={`https://fsrdata.s3.amazonaws.com/tracks/${currentTrack ? currentTrack.media_id : ''}`}/>
        ) : null;

        return (
            <div className="col-md-6">
                <div className="page-header">
                    <h4>Currently Playing {currentTrack ? `"${currentTrack.name}"` : ''}</h4>
                </div>
                {audioContainer}
            </div>
        );
    }
}

class Playlist extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentTrack: null
        };
    }

    componentWillMount() {
        const {playlistId} = this.props.params;
        this.props.fetchPlaylist(playlistId);
        this.props.fetchTracks();
    }

    addTrackIdsToPlaylist(ids) {
        const {playlist} = this.props;

        const currentTrackIds = _.map(playlist.tracks, (track) => track.id);
        const fullTrackIds = _.concat(ids, currentTrackIds);

        this.props.bulkAddTracksToPlaylist(playlist.id, fullTrackIds);
    }

    clearPlaylist() {
        const {playlist} = this.props;
        this.props.bulkAddTracksToPlaylist(playlist.id, []);
    }

    addTrackIdToPlaylist(track_id) {
        const {playlist} = this.props;
        this.props.addTrackToPlaylist(playlist.id, track_id);
    }

    removeTrackIdsFromPlaylist(track_id) {
        const {playlist} = this.props;
        this.props.delTrackFromPlaylist(playlist.id, track_id);
    }

    exportPlaylist(exportName) {
        const {playlist} = this.props;
        this.props.exportPlaylist(playlist.id, exportName);
    }

    updatePlaylist(update) {
        const {playlist} = this.props;
        this.props.updatePlaylist(playlist.id, update);
    }

    updateCurrentTrack(track) {
        this.setState(
            {
                currentTrack: track
            }
        );
    }

    render() {
        const {playlist, fetchingPlaylist} = this.props;
        const {currentTrack} = this.state;

        return (
            <div>
                <div className="container-fluid">
                    <div className="panel panel-default">
                        <div className="panel-body row">
                            <UpdatePlaylist
                                playlist={playlist}
                                loading={fetchingPlaylist}

                                exportPlaylist={(name) => this.exportPlaylist(name)}
                                updatePlaylist={(update) => this.updatePlaylist(update)}
                            />
                            <CurrentTrack currentTrack={currentTrack}/>
                        </div>
                        <div className="panel-body row">
                            <PlaylistContainer
                                playlist={playlist}
                                loading={fetchingPlaylist}

                                removeTrackIdsFromPlaylist={this.removeTrackIdsFromPlaylist.bind(this)}
                                clearPlaylist={this.clearPlaylist.bind(this)}
                                updateCurrentTrack={(track) => this.updateCurrentTrack(track)}
                            />
                            <TracklistContainer
                                tracks={this.props.tracks}
                                addTrackIdToPlaylist={this.addTrackIdToPlaylist.bind(this)}
                                addTracksToPlaylist={this.addTrackIdsToPlaylist.bind(this)}

                                updateTrack={this.props.updateTrack}
                                upvoteTrack={this.props.upvoteTrack}
                                downvoteTrack={this.props.downvoteTrack}

                                updateCurrentTrack={(track) => this.updateCurrentTrack(track)}
                                showNotification={() => console.log('dep')}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        fetchingPlaylist: state.playlist.isFetching,
        playlist: state.playlist.playlist,
        tracks: state.tracks.tracks
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPlaylist: (id) => {
            dispatch(fetchPlaylist(id));
        },
        updatePlaylist: (playlist_id, update) => {
            dispatch(updatePlaylist(playlist_id, update));
        },
        addTrackToPlaylist: (playlist_id, track_id) => {
            dispatch(addTrackToPlaylist(playlist_id, track_id));
        },
        delTrackFromPlaylist: (playlist_id, track_id) => {
            dispatch(delTrackFromPlaylist(playlist_id, track_id));
        },
        bulkAddTracksToPlaylist: (playlist_id, track_ids) => {
            dispatch(bulkAddTracksToPlaylist(playlist_id, track_ids));
        },
        exportPlaylist: (playlist_id, exportName) => {
            dispatch(exportPlaylist(playlist_id, exportName));
        },

        fetchTracks: () => {
            dispatch(fetchTracks());
        },
        updateTrack: (track_id, update) => {
            dispatch(updateTrack(track_id, update));
        },
        upvoteTrack: (track_id) => {
            dispatch(upvoteTrack(track_id));
        },
        downvoteTrack: (track_id) => {
            dispatch(downvoteTrack(track_id));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Playlist);