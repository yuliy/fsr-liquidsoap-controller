import React from 'react';
import { connect } from 'react-redux';

var t = require('tcomb-form');
var Form = t.form.Form;

import {fetchToken} from '../actions';

import Header from '../components/Header';

const LoginFormType = t.struct({
    username: t.String,
    password: t.String
});

const loginFormOptions = {
    fields: {
    }
};

export default class Login extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {fetchToken} = this.props;

        const handleLoginButtonSubmit = (e) => {
            e.preventDefault();

            const value = this.refs.form.getValue();
            if (!value) return;

            fetchToken(value.username, value.password);
        };

        return (
            <div>
                <div className="container-fluid">
                    <div className="panel panel-default">
                        <div className="panel-body row">
                            <Form
                                ref="form"
                                value={{
                                    username: '',
                                    password: ''
                                }}
                                type={LoginFormType}
                                options={loginFormOptions}
                            />
                            <button type="submit" className="btn btn-default" onClick={(e) => handleLoginButtonSubmit(e)}>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchToken: (username, password) => {
            dispatch(fetchToken(username, password));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);