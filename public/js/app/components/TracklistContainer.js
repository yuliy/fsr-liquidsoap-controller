import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import TagFactory from './form/TagFactory';

var t = require('tcomb-form');
var Form = t.form.Form;

const Track = t.struct({
    name: t.maybe(t.String),
    tags: t.list(t.String)
});

class TracklistItem extends React.Component {
    static propTypes = {
        track: React.PropTypes.shape({
            id: React.PropTypes.number.isRequired,
            name: React.PropTypes.string.isRequired,
            tags: React.PropTypes.array.isRequired,
            upvotes: React.PropTypes.number.isRequired,
            downvotes: React.PropTypes.number.isRequired
        }),
        isSelected: React.PropTypes.bool.isRequired,
        updateTrack: React.PropTypes.func.isRequired,
        upvoteTrack: React.PropTypes.func.isRequired,
        downvoteTrack: React.PropTypes.func.isRequired,
        addToSelection: React.PropTypes.func.isRequired,
        addTrackToPlaylist: React.PropTypes.func.isRequired,
        updateCurrentTrack: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
    }

    addTrackToPlaylist(e) {
        e.preventDefault();
        const {addTrackToPlaylist, track} = this.props;
        addTrackToPlaylist(track.id);
    }

    upvote(e) {
        e.preventDefault();
        const {upvoteTrack, track} = this.props;
        upvoteTrack(track.id);
    }

    downvote(e) {
        e.preventDefault();
        const {downvoteTrack, track} = this.props;
        downvoteTrack(track.id);
    }

    handleSelectButton(e) {
        e.preventDefault();
        const {track, addToSelection} = this.props;
        addToSelection(track.id);
    }

    handleUpdateCurrentTrack(e) {
        e.preventDefault();
        const {track, updateCurrentTrack} = this.props;
        updateCurrentTrack(track);
    }

    render() {
        const {track, updateTrack, isSelected} = this.props;

        const value = {
            name: track.name,
            tags: _.map(track.tags, (tag) => {
                return tag.label;
            })
        };

        const options = {
            fields: {
                tags: {
                    label: "Tags",
                    factory: TagFactory
                }
            }
        };

        let selectedButtonClasses = classNames("btn", "btn-default", "btn-sm", "btn-add-track", {'selected': isSelected});

        return (
            <li className="list-group-item">
                <div className="btn-group btn-group-right" role="group" aria-label="..." style={{paddingBottom: '10px'}}>
                    <button type="button" className="btn btn-default btn-sm btn-add-track" onClick={(e) => this.addTrackToPlaylist(e)}>
                        <span className="glyphicon glyphicon-plus" aria-hidden="true" /> Add
                    </button>
                    <button type="button" className={selectedButtonClasses} onClick={(e) => this.handleSelectButton(e)}>
                        <span className="glyphicon glyphicon-plus" aria-hidden="true" /> Select
                    </button>
                    <button type="button" className="btn btn-default btn-sm btn-play-track" onClick={(e) => this.handleUpdateCurrentTrack(e)}>
                        <span className="glyphicon glyphicon-play" aria-hidden="true" />
                    </button>
                    <button type="button" className="btn btn-default btn-sm btn-upvote-track" onClick={(e) => this.upvote(e)}>
                        <span className="glyphicon glyphicon-thumbs-up" aria-hidden="true" /> Upvote <span className="badge">{track.upvotes}</span>
                    </button>
                    <button type="button" className="btn btn-default btn-sm btn-downvote-track" onClick={(e) => this.downvote(e)}>
                        <span className="glyphicon glyphicon-thumbs-down" aria-hidden="true" /> Downvote <span className="badge">{track.downvotes}</span>
                    </button>
                </div>
                <div className="track-form-container">
                    <Form
                        ref="form"
                        value={value}
                        type={Track}
                        options={options}
                    />
                </div>
                <div className="btn-group" style={{paddingTop: '10px', width: '100%'}}>
                    <button className="btn btn-secondary" type="button" style={{width: '100%'}} onClick={(e) => updateTrack(e, track.id, this.refs.form.getValue())}>Update</button>
                </div>
            </li>
        );
    }
}

export default class TracklistContainer extends React.Component {
    static propTypes = {
        pageSize: React.PropTypes.number.isRequired,
        addTrackIdToPlaylist: React.PropTypes.func.isRequired,
        addTracksToPlaylist: React.PropTypes.func.isRequired,
        updateCurrentTrack: React.PropTypes.func.isRequired,
        showNotification: React.PropTypes.func.isRequired
    };

    static defaultProps = {
        pageSize: 8
    };

    constructor(props) {
        super(props);

        this.state = {
            currentPage: 0,
            currentFilter: '',
            currentSelection: [],
            currentMediaId: null
        };
    }

    updateTrackInfo(track) {
        const id = track.id;
        const {tracks} = this.state;

        let updatedTracks = _.map(tracks, (item) => {
            if (item.id === id) {
                return track
            }

            return item;
        });

        this.setState(
            {
                tracks: updatedTracks
            }
        );
    }

    updateTrack(e, trackId, update) {
        e.preventDefault();
        this.props.updateTrack(trackId, update);
    }

    addToSelection(trackId) {
        const {currentSelection} = this.state;

        if (_.indexOf(currentSelection, trackId) !== -1) {
            this.setState(
                {
                    currentSelection: _.without(currentSelection, trackId)
                }
            );
        } else {
            this.setState(
                {
                    currentSelection: _.concat(currentSelection, trackId)
                }
            );
        }
    }

    addAllToSelection(e) {
        e.preventDefault();
        const {tracks} = this.props;
        this.setState(
            {
                currentSelection: _.map(tracks, (track) => track.id)
            }
        );
    }

    clearAllSelected(e) {
        e.preventDefault();
        this.setState(
            {
                currentSelection: []
            }
        );
    }

    updatePlaylistTracks(e) {
        const {currentSelection} = this.state;
        const {addTracksToPlaylist} = this.props;
        e.preventDefault();
        addTracksToPlaylist(currentSelection);
        this.setState(
            {
                currentSelection: []
            }
        );
    }

    updateCurrentTrack(track) {
        const {updateCurrentTrack} = this.props;
        updateCurrentTrack(track);
    }

    changePage(e, quantity) {
        e.preventDefault();

        const {pageSize, tracks} = this.props;
        const {currentPage} = this.state;

        let totalEntries = tracks.length;
        let pageCount = Math.floor(totalEntries / pageSize);

        let updatedPage = currentPage + quantity;

        if (updatedPage <= pageCount && updatedPage >= 0) {
            this.setState(
                {
                    currentPage: updatedPage
                }
            );
        }
    }

    handleFilter(e) {
        this.setState(
            {
                currentFilter: e.currentTarget.value.toLowerCase()
            }
        );
    }

    render() {
        const {pageSize, tracks, addTrackIdToPlaylist} = this.props;
        const {currentPage, currentFilter, currentSelection} = this.state;

        let filteredTracklist = _.filter(tracks, (entry) => {
            if (!currentFilter) return true;

            return entry.name.toLowerCase().indexOf(currentFilter) >= 0;
        });
        let slicedTracklist = _.slice(filteredTracklist, currentPage * pageSize, currentPage * pageSize + pageSize);
        let lister = slicedTracklist.map((card, i) => {
            return (
                <TracklistItem key={i}
                               track={card}
                               isSelected={_.indexOf(currentSelection, card.id) !== -1}
                               updateTrack={this.updateTrack.bind(this)}
                               upvoteTrack={this.props.upvoteTrack}
                               downvoteTrack={this.props.downvoteTrack}
                               addToSelection={this.addToSelection.bind(this)}
                               addTrackToPlaylist={addTrackIdToPlaylist}
                               updateCurrentTrack={(track) => this.updateCurrentTrack(track)}
                />
            );
        });

        let totalEntries = filteredTracklist.length;
        let pageCount = Math.floor(totalEntries / pageSize);

        return (
            <div className="col-md-6">
                <div className="page-header">
                    <h2>All Tracks <small>{tracks.length} Tracks</small></h2>
                </div>

                <div className="input-group columnControls">
                    <span className="input-group-addon" id="basic-addon1">Filter</span>
                    <input type="text" className="form-control" placeholder="" aria-describedby="basic-addon1" onChange={(e) => this.handleFilter(e)} />
                </div>

                <div className="btn-group btn-group-right columnControls" role="group" aria-label="...">
                    <button type="button" className="btn btn-default" onClick={(e) => this.updatePlaylistTracks(e)}>
                        <span className="glyphicon glyphicon-plus" aria-hidden="true" /> Add Selected
                    </button>
                    <button type="button" className="btn btn-default" onClick={(e) => this.clearAllSelected(e)}>
                        <span className="glyphicon glyphicon-fire" aria-hidden="true" /> Clear Selected
                    </button>
                    <button type="button" className="btn btn-default" onClick={(e) => this.addAllToSelection(e)}>
                        <span className="glyphicon glyphicon-globe" aria-hidden="true" /> Select All
                    </button>
                </div>

                <ul id="current-playlist" className="list-group">
                    {lister}
                </ul>

                <div className="pager">
                    <div>
                        {`Displaying page ${currentPage + 1} of ${pageCount+1}`}
                    </div>
                    <div style={{float: 'right'}}>
                        <a href="#" style={{paddingRight: '10px'}} onClick={(e) => this.changePage(e, -1)}>Prev</a>
                        <a href="#" onClick={(e) => this.changePage(e, 1)}>Next</a>
                    </div>
                </div>
            </div>
        );
    }
}