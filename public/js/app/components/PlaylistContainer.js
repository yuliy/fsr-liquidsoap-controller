import React from 'react';
import Loader from 'react-loader-advanced';

class PlaylistItem extends React.Component {
    static propTypes = {
        track: React.PropTypes.shape({
            id: React.PropTypes.number.isRequired,
            name: React.PropTypes.string.isRequired,
            //tags: React.PropTypes.array.isRequired, @todo fix this
            upvotes: React.PropTypes.number.isRequired,
            downvotes: React.PropTypes.number.isRequired
        }),
        removeTrackFromPlaylist: React.PropTypes.func.isRequired,
        updateCurrentTrack: React.PropTypes.func.isRequired
    };

    handleRemoveButton(e) {
        e.preventDefault();
        const {track, removeTrackFromPlaylist} = this.props;
        removeTrackFromPlaylist(track.id);
    }

    handleUpdateCurrentTrack(e) {
        e.preventDefault();
        const {track, updateCurrentTrack} = this.props;
        updateCurrentTrack(track);
    }

    render() {
        const {track} = this.props;

        return (
            <li className="list-group-item" key={track.id}>
                <div className="btn-group btn-group-right" role="group" aria-label="..." style={{paddingBottom: '10px'}}>
                    <button type="button" className="btn btn-default btn-sm" onClick={(e) => this.handleRemoveButton(e)}>
                        <span className="glyphicon glyphicon-minus" aria-hidden="true" /> Remove
                    </button>
                    <button type="button" className="btn btn-default btn-sm btn-play-track" onClick={(e) => this.handleUpdateCurrentTrack(e)}>
                        <span className="glyphicon glyphicon-play" aria-hidden="true" />
                    </button>
                </div>

                <div>
                    {track.name}
                </div>
            </li>
        )
    }
}

export default class PlaylistContainer extends React.Component {
    static propTypes = {
        playlist: React.PropTypes.shape({
            //id: React.PropTypes.number.isRequired, @todo why doesn't this work?
            tracks: React.PropTypes.array.isRequired,
            name: React.PropTypes.string.isRequired
        }),
        pageSize: React.PropTypes.number,
        loading: React.PropTypes.bool.isRequired,
        clearPlaylist: React.PropTypes.func.isRequired,
        removeTrackIdsFromPlaylist: React.PropTypes.func.isRequired,
        updateCurrentTrack: React.PropTypes.func.isRequired
    };

    static defaultProps = {
        tracks: [],
        pageSize: 8,
        currentFilter: '',
        loading: false
    };

    constructor(props) {
        super(props);

        this.state = {
            currentPage: 0
        };
    }

    clearPlaylist(e) {
        e.preventDefault();
        this.props.clearPlaylist();
    }

    removeTrackFromPlaylist(trackId) {
        this.props.removeTrackIdsFromPlaylist([trackId]);
    }

    changePage(e, quantity) {
        e.preventDefault();

        const {playlist, pageSize} = this.props;
        const {currentPage} = this.state;

        let totalEntries = playlist.tracks.length;
        let pageCount = Math.floor(totalEntries / pageSize);

        let updatedPage = currentPage + quantity;

        if (updatedPage <= pageCount && updatedPage >= 0) {
            this.setState(
                {
                    currentPage: updatedPage
                }
            );
        }
    }

    handleFilter(e) {
        this.setState(
            {
                currentFilter: e.currentTarget.value.toLowerCase()
            }
        );
    }

    render() {
        const {playlist, pageSize, loading} = this.props;
        const {currentPage, currentFilter} = this.state;

        let filteredTracklist = _.filter(playlist.tracks, (entry) => {
            if (!currentFilter) return true;

            return entry.name.toLowerCase().indexOf(currentFilter) >= 0;
        });

        let slicedPlaylist = _.slice(filteredTracklist, currentPage * pageSize, currentPage * pageSize + pageSize);
        let lister = slicedPlaylist.map((card, i) => {
            return (
                <PlaylistItem
                    key={i}
                    track={card}
                    removeTrackFromPlaylist={this.removeTrackFromPlaylist.bind(this)}
                    updateCurrentTrack={(track) => this.props.updateCurrentTrack(track)}
                />
            );
        });

        let totalEntries = filteredTracklist.length;
        let pageCount = Math.floor(totalEntries / pageSize);

        return (
            <div className="col-md-6">
                <Loader show={loading} message={'loading'}>
                    <div className="page-header">
                        <h2>Current Tracks <small>{playlist.tracks.length} Tracks</small></h2>
                    </div>

                    <div className="input-group columnControls">
                        <span className="input-group-addon" id="basic-addon1">Filter</span>
                        <input type="text" className="form-control" placeholder="" aria-describedby="basic-addon1" onChange={(e) => this.handleFilter(e)} />
                    </div>

                    <div className="btn-group btn-group-right columnControls" role="group" aria-label="...">
                        <button type="button" className="btn btn-default" onClick={(e) => this.clearPlaylist(e)}>
                            <span className="glyphicon glyphicon-fire" aria-hidden="true" /> Clear Playlist
                        </button>
                    </div>

                    <ul className="list-group">
                        {lister}
                    </ul>

                    <nav>
                        <div style={{textAlign: "center"}}>
                            {`Displaying page ${currentPage + 1} of ${pageCount+1}`}
                        </div>
                        <ul className="pagination">
                            <li>
                                <a href="#" aria-label="Previous" onClick={(e) => this.changePage(e, -1)}>
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" aria-label="Next" onClick={(e) => this.changePage(e, 1)}>
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </Loader>
            </div>
        );
    }
}