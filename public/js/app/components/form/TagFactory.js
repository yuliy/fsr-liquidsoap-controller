import React from 'react';
import TagsInput from 'react-tagsinput';

var t = require('tcomb-form');

export default class TagFactory extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hasError: false,
            value: this.props.value
        }
    }

    componentWillReceiveProps(props) {
        this.setState(
            {
                value: props.value
            }
        );
    }

    shouldComponentUpdate (nextProps, nextState) {
        return nextState.value !== this.state.value ||
            nextState.hasError !== this.state.hasError ||
            nextProps.value !== this.props.value ||
            nextProps.options !== this.props.options ||
            nextProps.onChange !== this.props.onChange;
    }

    onChange(value) {
        this.setState({value: value}, function () {
            this.props.onChange(value);
        }.bind(this));
    }

    getValidationOptions() {
        return {
            path   : this.props.ctx.path,
            context: t.mixin(t.mixin({}, this.props.context || this.props.ctx.context), {options: this.props.options})
        };
    }

    validate() {
        const {value} = this.state;
        const result = t.validate(value, this.props.type, this.getValidationOptions());
        this.setState({hasError: !result.isValid()});
        return result;
    }

    render() {
        const {value} = this.state;

        return (
            <div>
                <label className="control-label">{this.props.ctx.label}</label>
                <TagsInput value={value} onChange={this.onChange.bind(this)} />
            </div>
        );
    }
}