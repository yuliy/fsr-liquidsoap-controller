import React, {Component, PropTypes} from 'react';
import Crouton from 'react-crouton';

export default class Notification extends Component {
    static displayName = 'Notification';
    static propTypes = {
        notifications: PropTypes.arrayOf(PropTypes.shape({
            id     : PropTypes.number.isRequired,
            message: PropTypes.string.isRequired,
            type   : PropTypes.string
        })).isRequired
    };

    shouldComponentUpdate(nextProps) {
        return this.props.notifications.length !== nextProps.notifications.length;
    }

    render() {
        const {notifications} = this.props;
        if (!notifications.length) return null;
        const n = notifications[notifications.length - 1];

        return (
            <div>
                <Crouton timeout={5000} id={n.id} message={n.message} type={n.type || 'error'}/>
            </div>

        );
    }
}