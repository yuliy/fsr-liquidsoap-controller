import React from 'react';
import {Link} from 'react-router';

export default class Header extends React.Component {
    static propTypes = {
        handleLogout: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {handleLogout} = this.props;

        const onClickLogout = (e) => {
            e.preventDefault();

            handleLogout();
        };

        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            <li><Link to={"/"}>Home</Link></li>
                            <li><Link to={"/liquidsoap"}>Liquid Soap</Link></li>
                            <li><a onClick={(e) => onClickLogout(e)}>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}