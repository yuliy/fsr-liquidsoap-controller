var passport = require('koa-passport');
var R = require('ramda');
var LocalStrategy = require('passport-local');

var orm = require('./orm');
var secure_service = require('./services/secure_service');

function getUserWithEmail(email) {
    return orm.User.where('username', '=', email).fetch();
}

function getUserWithId(id) {
    return orm.User.where('id', '=', id).fetch();
}

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    getUserWithId(id).nodeify(done);
});

passport.use(new LocalStrategy(
    {
        usernameField: 'username'
    },
    (email, password, done) => {
        getUserWithEmail(email)
            .then((u) => {
                if (u) {
                    secure_service.bcryptCompare(password, u.get('password')).then((matches) => {
                        if (matches) {
                            done(null, R.dissoc('password', u));
                        } else {
                            done(null, false);
                        }
                    });
                } else {
                    done(null, false);
                }
            });
    }));