var _ = require('lodash');

var orm = require('../orm');
var s3_service = require('./s3_service');

const getPlaylist = (id) => {
    return orm.Playlist.where('id', '=', id)
        .fetch({
            withRelated: [
                {
                    tracks: (query) => {
                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                    }
                }
            ],
            require: true
        });
};

const getPlaylists = () => {
    return orm.Playlist
        .fetchAll({
            withRelated: [
                {
                    tracks: (query) => {
                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                    }
                }
            ]
        });
};

const createPlaylist = (createData) => {
    return orm.Playlist.forge(createData).save();
};

const updatePlaylist = (id, updateData) => {
    return orm.Playlist.where('id', '=', id)
        .fetch({
            withRelated: [
                {
                    tracks: (query) => {
                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                    }
                }
            ],
            require: true
        })
        .then((playlist) => {
            return playlist.set(updateData).save();
        });
};

const addTrackToPlaylist = (id, track_id) => {
    return orm.Playlist.where('id', '=', id)
        .fetch({
            withRelated: [
                {
                    tracks: (query) => {
                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                    }
                }
            ],
            require: true
        })
        .then(function (playlist) {
            return playlist.tracks()
                .attach(
                    [track_id]
                )
                .then(function () {
                    return orm.Playlist.where('id', '=', id)
                        .fetch({
                            withRelated: [
                                {
                                    tracks: (query) => {
                                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                                    }
                                }
                            ],
                            require: true
                        });
                });
        });
};

const deleteTrackFromPlaylist = (id, track_id) => {
    return orm.Playlist.where('id', '=', id)
        .fetch({
            withRelated: [
                {
                    tracks: (query) => {
                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                    }
                }
            ],
            require: true
        })
        .then(function (playlist) {
            return playlist.tracks()
                .detach(
                    [track_id]
                )
                .then(function () {
                    return orm.Playlist.where('id', '=', id)
                        .fetch({
                            withRelated: [
                                {
                                    tracks: (query) => {
                                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                                    }
                                }
                            ],
                            require: true
                        });
                });
        });
};

const updatePlaylistTracks = (id, track_ids) => {
    return orm.Playlist.where('id', '=', id)
        .fetch({
            withRelated: [
                {
                    tracks: (query) => {
                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                    }
                }
            ],
            require: true
        })
        .then(function (playlist) {
            return orm.Track.where('id', 'in', track_ids)
                .fetchAll()
                .then(function (tracks) {
                    var actualIds = tracks.map(function (item) {
                        return item.id;
                    });

                    return playlist.tracks().detach()
                        .then(function () {
                            return playlist.tracks().attach(actualIds)
                                .then(function (updated_playlist) {
                                    return orm.Playlist.where('id', '=', id)
                                        .fetch({
                                            withRelated: [
                                                {
                                                    tracks: (query) => {
                                                        query.select('track.id', 'track.media_id', 'track.name', 'track.upvotes', 'track.downvotes');
                                                    }
                                                }
                                            ],
                                            require: true
                                        });
                                });
                        });
                });
        });
};

const exportPlaylist = (id, name) => {
    return orm.Playlist.where('id', '=', id)
        .fetch({
            withRelated: ['tracks'],
            require: true
        })
        .then(function (playlist) {
            var tracks = playlist.related('tracks');
            var trackS3Links = tracks.map(function (item) {
                return item.getM3ULine();
            });
            var fullFile = _.join(trackS3Links, "\n");

            return s3_service.fsrPlaylist.writeFile(name + ".m3u", fullFile, {
                ACL: 'public-read'
            });
        })
        .then(() => {
            return "https://fsrplaylists.s3.amazonaws.com/" + name + ".m3u";
            //if (reload !== undefined) {
            //    var client = net.connect({port: 1234},
            //        function () {
            //            client.write(reload + '.reload\r\n');
            //        }
            //    );
            //    var closed = false;
            //    client.on('data', function (data) {
            //        if (!closed) {
            //            var res = data.toString();
            //
            //            if (_.startsWith(res, "OK")) {
            //                response.status(200).send({
            //                    message: "Exported Playlist and Reloaded Liquidsoap Playlist: " + reload,
            //                    playlistUri: playlistURI
            //                });
            //            } else {
            //                response.status(200).send({
            //                    message: "Exported Playlist but Reload Failed with: " + res,
            //                    playlistUri: playlistURI
            //                });
            //            }
            //
            //            client.write("quit\r\n");
            //            closed = true;
            //        }
            //    });
            //    client.on('error', function () {
            //        console.log(arguments);
            //    });
            //    client.on('end', function () {
            //        console.log('disconnected from server');
            //    });
            //} else {
            //    response.status(200).send({
            //        message: "Exported Playlist",
            //        playlistUri: playlistURI
            //    });
            //}
        });
};

exports.getPlaylist = getPlaylist;
exports.getPlaylists = getPlaylists;
exports.createPlaylist = createPlaylist;
exports.updatePlaylist = updatePlaylist;
exports.addTrackToPlaylist = addTrackToPlaylist;
exports.deleteTrackFromPlaylist = deleteTrackFromPlaylist;
exports.updatePlaylistTracks = updatePlaylistTracks;
exports.exportPlaylist = exportPlaylist;