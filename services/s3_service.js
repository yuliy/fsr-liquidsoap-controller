var S3FS = require('s3fs');
var s3Utils = require('s3fs/lib/utils');

var config = require('../config');

/**
 * Setup S3 clients
 */
var fsrData = new S3FS(
    'fsrdata',
    {
        accessKeyId: config.s3.aws_token,
        secretAccessKey: config.s3.aws_secret,
        region: 'us-west-2',
        acl: 'public-read'
    }
);

fsrData.copyFile = function (sourceFile, destinationFile, callback) {
    var self = this;
    var promise = new Promise(function (resolve, reject) {
        self.s3.copyObject({
            Bucket: self.bucket,
            Key: s3Utils.toKey(s3Utils.joinPaths(self.path + s3Utils.toKey(destinationFile))),
            CopySource: [self.bucket, s3Utils.toKey(s3Utils.joinPaths(self.path + s3Utils.toKey(sourceFile)))].join('/'),
            ACL: 'public-read'
        }, function (err, data) {
            if (err) {
                return reject(err);
            }
            return resolve(data);
        });
    });

    if (!callback) {
        return promise;
    }

    promise.then(function (data) {
        callback(null, data);
    }, function (reason) {
        callback(reason);
    });
};

var fsrPlaylist = new S3FS(
    'fsrplaylists',
    {
        accessKeyId: config.s3.aws_token,
        secretAccessKey: config.s3.aws_secret,
        region: 'us-west-2'
    }
);

exports.fsrData = fsrData;
exports.fsrPlaylist = fsrPlaylist;