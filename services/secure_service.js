var bcrypt = require('bcrypt');
var bluebird = require('bluebird');

const bcryptHash = (value, saltLength) => {
    return bluebird.Promise.fromNode(callback => {
        bcrypt.hash(value, saltLength, callback);
    });
};

const bcryptCompare = (value, hash) => {
    return bluebird.Promise.fromNode(callback => {
        bcrypt.compare(value, hash, callback);
    });
};

exports.bcryptHash = bcryptHash;
exports.bcryptCompare = bcryptCompare;