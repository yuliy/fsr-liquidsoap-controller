var _ = require('lodash');

var orm = require('../orm');
var s3_service = require('./s3_service');

const buildHeadMediaFilesPromises = (s3Client, media_id, prefix) => {
    if (prefix === undefined) {
        prefix = '';
    }

    return [
        s3Client.headObject(prefix + '/tracks/' + media_id).then(
            () => {
                return {
                    type: 'track',
                    mediaId: media_id,
                    path: prefix + '/tracks/' + media_id
                };
            }
        ).catch(() => {
        }),
        s3Client.headObject(prefix + '/metadata/' + media_id + ".meta").then(
            () => {
                return {
                    type: 'metadata',
                    mediaId: media_id,
                    path: prefix + '/metadata/' + media_id + ".meta"
                };
            }
        ).catch(() => {
        }),
        s3Client.headObject(prefix + '/radio/' + media_id + "_radio.mp3").then(
            () => {
                return {
                    type: 'radio',
                    mediaId: media_id,
                    path: prefix + '/radio/' + media_id + "_radio.mp3"
                };
            }
        ).catch(() => {
        }),
        s3Client.headObject(prefix + '/images/' + media_id + ".png").then(
            () => {
                return {
                    type: 'image',
                    mediaId: media_id,
                    path: prefix + '/images/' + media_id + ".png"
                };
            }
        ).catch(() => {
        }),
        s3Client.headObject(prefix + '/image_metadata/' + media_id + ".png.meta").then(
            () => {
                return {
                    type: 'image_metadata',
                    mediaId: media_id,
                    path: prefix + '/image_metadata/' + media_id + ".png.meta"
                };
            }
        ).catch(() => {
        })
    ];
};

/**
 * @param id
 * @returns {Promise.<T>}
 */
const getTrack = (id) => {
    return orm.Track.where('id', '=', id)
        .fetch({
            withRelated: ['tags'],
            require: true
        });
};

/**
 * @returns {Promise.<T>}
 */
const getTracks = () => {
    return orm.Track
        .query((qb) => {
            qb.select('id', 'media_id', 'name', 'upvotes', 'downvotes');
        })
        .fetchAll({
            withRelated: ['tags'],
            require: true
        })
        .then((rows) => {
            return rows.sortBy(
                ['name']
            );
        });
};

const updateTrack = (id, track_data) => {
    return orm.Track.where('id', '=', id)
        .fetch({
            withRelated: ['tags'],
            require: true
        })
        .then(function (track) {
            var filteredUpdate = _.pick(track_data, ['name']);
            var parsedTags = _.pick(track_data, ['tags']).tags;

            return orm.Tag.where('label', 'in', parsedTags)
                .fetchAll()
                .then(function (existingTags) {
                    existingTags = existingTags.toJSON();

                    var existingTagLabels = _.map(existingTags, (item) => {
                        return item.label;
                    });
                    var existingTagIds = _.map(existingTags, (item) => {
                        return item.id;
                    });

                    var doNotExist = _.difference(parsedTags, existingTagLabels);

                    var toCreate = _.map(doNotExist, function (item) {
                        return {label: item}
                    });

                    var models = _.map(toCreate, (createData) => {
                        return new orm.Tag(createData).save().then(function (model) {
                            return model.id;
                        });
                    });

                    return Promise.all(
                        models
                        )
                        .then(function (ids) {
                            return _.concat(ids, existingTagIds);
                        });
                })
                .then((ids) => {
                    return track.tags().detach()
                        .then(() => {
                            return Promise.all(
                                [
                                    track.tags().attach(ids)
                                ]
                            );
                        });
                })
                .then(() => {
                    return track.refresh(
                        {
                            withRelated: ['tags'],
                            require: true
                        }
                    );
                })
                .then(() => {
                    return track.set(filteredUpdate).save();
                });
        });
};

/**
 * @param media_id
 * @returns {Promise.<T>}
 */
const headTrack = (media_id) => {
    return orm.Track.where('media_id', '=', media_id)
        .fetch()
        .then((track) => {
            return Promise.all(
                buildHeadMediaFilesPromises(s3_service.fsrData, media_id)
                )
                .then((retrieved) => {
                    retrieved = _.filter(retrieved, (item) => {
                        return item !== undefined && item !== null;
                    });

                    var retrievedKeys = _.map(retrieved, (item) => {
                        if (item !== undefined) return item.type;
                    });
                    var missing = _.difference(['track', 'metadata', 'radio', 'image', 'image_metadata'], retrievedKeys);

                    var message = "Track with media_id " + media_id + " does not exist";
                    if (_.isNil(track)) {
                        message = "Track with media_id " + media_id + " does not exist";
                    } else {
                        message = "Track with media_id " + media_id + " does exist";
                    }

                    return {
                        message: message,
                        track: track,
                        files: retrieved,
                        missingFiles: missing
                    };
                });
        });
};

/**
 * @param media_id
 * @returns {Promise.<T>}
 */
const upsertTrackFiles = (media_id) => {
    return orm.Track.where('media_id', '=', media_id)
        .fetch()
        .then((track) => {
            return headTrack(media_id)
                .then((info) => {
                    if (info.missingFiles.length > 0) {
                        throw "Missing Data Files";
                    }
                })
                .then(
                    () => {
                        return Promise.all(
                            [
                                s3_service.fsrData.readFile('/metadata/' + media_id + ".meta")
                                    .then((data) => {
                                        var buf = data.Body;
                                        return {
                                            type: "track",
                                            record: JSON.parse(buf.toString())
                                        }
                                    }),
                                s3_service.fsrData.readFile('/image_metadata/' + media_id + ".png.meta")
                                    .then((data) => {
                                        var buf = data.Body;
                                        return {
                                            type: "image",
                                            record: JSON.parse(buf.toString())
                                        }
                                    })
                            ]
                        );
                    }
                )
                .then(
                    (metadatas) => {
                        const trackMetadata = _.head(
                            _.filter(metadatas,
                                (item) => {
                                    return item.type === "track";
                                })
                        ).record;
                        const imageMetadata = _.head(
                            _.filter(metadatas,
                                (item) => {
                                    return item.type === "image";
                                })
                        ).record;

                        if (_.isNil(track)) {
                            return new orm.Track(
                                {
                                    media_id: media_id,
                                    name: trackMetadata.trackname,
                                    metadata: trackMetadata,
                                    image_metadata: imageMetadata
                                }
                            ).save();
                        } else {
                            return track.set(
                                {
                                    metadata: trackMetadata,
                                    image_metadata: imageMetadata
                                }
                            ).save();
                        }
                    }
                )
                .then((track) => {
                    return {
                        message: "Upserted Track with media_id " + track.get('media_id'),
                        mediaId: track.get('media_id'),
                        wasNew: _.isNil(track)
                    };
                });
        });
};

/**
 * @param id
 * @returns {Promise.<T>}
 */
var upvoteTrack = (id) => {
    return orm.Track.where('id', '=', id)
        .fetch({
            withRelated: ['tags'],
            require: true
        })
        .then((track) => {
            return track.save(
                {
                    upvotes: track.get("upvotes") + 1
                },
                {
                    patch: true
                }
            );
        })
        .catch((reason) => {
            throw reason;
        });
};

/**
 * @param id
 * @returns {Promise.<T>}
 */
var downvoteTrack = (id) => {
    return orm.Track.where('id', '=', id)
        .fetch({
            withRelated: ['tags'],
            require: true
        })
        .then((track) => {
            return track.save(
                {
                    downvotes: track.get("downvotes") + 1
                },
                {
                    patch: true
                }
            );
        })
        .catch((reason) => {
            throw reason;
        });
};

exports.getTrack = getTrack;
exports.getTracks = getTracks;
exports.updateTrack = updateTrack;
exports.headTrack = headTrack;
exports.upsertTrackFiles = upsertTrackFiles;
exports.upvoteTrack = upvoteTrack;
exports.downvoteTrack = downvoteTrack;